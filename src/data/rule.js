export default class Rule {
  static empty() {
    return new Rule("", "", "", "", "");
  }

  constructor(state, symbol, nextState, nextSymbol, direction) {
    this.state = state;
    this.symbol = symbol;
    this.nextState = nextState;
    this.nextSymbol = nextSymbol;
    this.direction = direction;

    this.breakpoint = false;
  }

  get condition() {
    return {
      state: this.state,
      symbol: this.symbol
    };
  }

  get transition() {
    return {
      state: this.nextState,
      symbol: this.nextSymbol,
      direction: this.direction
    };
  }

  toggleBreakpoint() {
    this.breakpoint = !this.breakpoint;
    console.log(this.breakpoint);

    return this.breakpoint;
  }
}
