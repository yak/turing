export default class Cell {
  static real(value, key) {
    return new Cell(value, key);
  }

  static virtual(key) {
    return new Cell("¤", key);
  }

  constructor(value, key) {
    this.value = value;
    this.key = key;
  }

  get symbol() {
    return this.value;
  }

  // set symbol(value) {
  //   this.value = value;
  // }

  get isVirtual() {
    return this.value === "¤";
  }

  get isReal() {
    return this.value !== "¤";
  }
}
