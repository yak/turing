export const EMPTY = "¤";

export const ENTRY = "entry";
export const FAIL = "fail";
export const SUCCESS = "success";

export const LEFT = "left";
export const RIGHT = "right";
export const NONE = "none";
