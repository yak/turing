import { writable, derived, get } from "svelte/store";
import { Cell, Rule } from "data";
import { ENTRY, SUCCESS, FAIL, LEFT, RIGHT, NONE, EMPTY } from "consts";

export const head = writable(0);
export const state = writable(ENTRY);

export const rules = writable([
  new Rule(ENTRY, "1", ENTRY, "1", RIGHT),
  new Rule(ENTRY, "2", ENTRY, "2", RIGHT),
  new Rule(ENTRY, "3", ENTRY, "3", RIGHT),
  new Rule(ENTRY, "4", ENTRY, "4", RIGHT),
  new Rule(ENTRY, "5", ENTRY, "5", RIGHT),
  new Rule(ENTRY, "6", ENTRY, "6", RIGHT),
  new Rule(ENTRY, "7", ENTRY, "7", RIGHT),
  new Rule(ENTRY, "8", ENTRY, "8", RIGHT),
  new Rule(ENTRY, "9", ENTRY, "9", RIGHT),
  new Rule(ENTRY, "0", ENTRY, "0", RIGHT),

  new Rule(ENTRY, EMPTY, "inc", EMPTY, LEFT),

  new Rule("inc", "8", "back", "9", LEFT),
  new Rule("inc", "7", "back", "8", LEFT),
  new Rule("inc", "6", "back", "7", LEFT),
  new Rule("inc", "5", "back", "6", LEFT),
  new Rule("inc", "4", "back", "5", LEFT),
  new Rule("inc", "3", "back", "4", LEFT),
  new Rule("inc", "2", "back", "3", LEFT),
  new Rule("inc", "1", "back", "2", LEFT),
  new Rule("inc", "0", "back", "1", LEFT),

  new Rule("inc", EMPTY, SUCCESS, "1", NONE),

  new Rule("back", "0", "back", "0", LEFT),
  new Rule("back", "1", "back", "1", LEFT),
  new Rule("back", "2", "back", "2", LEFT),
  new Rule("back", "3", "back", "3", LEFT),
  new Rule("back", "4", "back", "4", LEFT),
  new Rule("back", "5", "back", "5", LEFT),
  new Rule("back", "6", "back", "6", LEFT),
  new Rule("back", "7", "back", "7", LEFT),
  new Rule("back", "8", "back", "8", LEFT),
  new Rule("back", "9", "back", "9", LEFT),

  new Rule("back", EMPTY, SUCCESS, EMPTY, RIGHT)
]);

export const cells = writable([
  Cell.real("1", 101),
  Cell.real("1", 102),
  Cell.real("1", 103),
  Cell.real("1", 104)
]);

export const key = writable(0);

export const steps = writable(0);
export const viewedStep = writable(0);

export const history = writable([
  {
    steps: get(steps),
    cells: get(cells).slice(),
    state: get(state),
    head: get(head)
  }
]);

export const current = derived(
  [cells, head],
  ([$cells, $head]) => $cells[$head].value
);

export const states = derived(rules, $rules => {
  const states = [
    ...$rules.map(rule => rule.transition.state),
    ...$rules.map(rule => rule.condition.state)
  ];

  const temp = {};

  for (var i = 0; i < states.length; i++) {
    temp[states[i]] = true;
  }

  temp[SUCCESS] = true;
  temp[FAIL] = true;

  return Object.keys(temp);
});
