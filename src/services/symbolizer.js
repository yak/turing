import { key } from "stores";
import Cell from "data/cell";

export default class Symbolizer {
  constructor() {
    key.subscribe(value => (this._key = value));
  }

  real(value) {
    return Cell.real(value, this.key);
  }

  virtual() {
    return Cell.virtual(this.key);
  }

  get key() {
    key.update(n => (n += 1));
    return this._key;
  }

  reset() {
    key.set(0);
  }
}
