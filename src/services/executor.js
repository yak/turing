import { rules, state, cells, head, steps, history } from "stores";
import { FAIL, SUCCESS } from "consts";
import Symbolizer from "services/symbolizer";

export default class Executor {
  constructor() {
    this.symbolizer = new Symbolizer();

    rules.subscribe(value => (this.rules = value));
    state.subscribe(value => (this.state = value));
    cells.subscribe(value => (this.cells = value));
    head.subscribe(value => (this.head = value));
    steps.subscribe(value => (this.steps = value));
  }

  run() {
    for (;;) {
      if (this.state === FAIL) break;
      if (this.state === SUCCESS) break;

      const rule = this.findRule();

      if (rule) {
        if (rule.breakpoint) break;

        const transition = rule.transition;

        if (transition) {
          this.apply(transition);
        } else {
          this.fail();
        }
      } else {
        this.fail();
      }
    }
  }

  fail() {
    state.set(FAIL);
  }

  step() {
    const transition = this.findTransition();

    if (transition) {
      this.apply(transition);
    } else {
      this.fail();
    }
  }

  findRule() {
    return this.rules.find(
      ({ condition: { state, symbol } }) =>
        state == this.state && symbol == this.cells[this.head].symbol
    );
  }

  findTransition() {
    const rule = this.findRule();
    return rule ? rule.transition : null;
  }

  apply({ state, symbol, direction }) {
    this.write(symbol);
    this.restate(state);
    this.move(direction);

    steps.update(n => (n += 1));

    this.snap();
  }

  snap() {
    history.update(n => [
      {
        steps: this.steps,
        cells: this.cells.slice(0),
        state: this.state,
        head: this.head
      },
      ...n.slice(n.length - this.steps)
    ]);
  }

  restate(to) {
    state.set(to);
  }

  write(to) {
    cells.update(n => {
      n.splice(this.head, 1, this.symbolizer.real(to));
      return n;
    });
  }

  move(direction) {
    if (direction == "left") this.moveLeft();
    if (direction == "right") this.moveRight();
  }

  moveLeft() {
    if (this.head === 0) {
      cells.update(n => [this.symbolizer.virtual(), ...n]);
    } else {
      head.update(n => (n -= 1));
    }

    if (this.cells[this.cells.length - 1].isVirtual) {
      cells.update(n => {
        n.pop();
        return n;
      });
    }
  }

  moveRight() {
    if (this.head === this.cells.length - 1) {
      cells.update(n => [...n, this.symbolizer.virtual()]);
    }

    if (this.cells[0].isVirtual) {
      cells.update(n => {
        n.shift();
        return n;
      });
    } else {
      head.update(n => (n += 1));
    }
  }
}
